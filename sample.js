/*
  Generates Items on the screen
*/
var setup = function (container, items) {
  var body = $(container || "body");
  var samples = [];

  for (var index = 0, count = items || 100; index < count; index++) {
    var sample = $("<img/>");

    sample.addClass("sample");
    sample.text(index+1);
    sample.attr({
      "id": "item-" + index,
      "src": "http://placehold.it/300x300"
    });

    samples.push(sample);
   }

   body.append(samples);
};