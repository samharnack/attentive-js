var blank = 'data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///ywAAAAAAQABAAACAUwAOw==';
var lazyload = function (element) {
  var speed = 1000;
  var img = $(element);
  var src = img.data("src") || img.attr("data-src");
  var oncomplete = callback(img);

  // remove data-src
  img.removeAttr("data-src");
  
  //Load Image In Memory
  $("<img/>")
  //Set The Src To A Blank Image To Trigger The Load Event For The Real Image
  .attr("src", blank)
  //Listen For The Load
  .one("load", oncomplete)
  //Listen For An Error
  .one("error", oncomplete)
  //Finally Load The Real Image
  .attr( "src", src );
  
  function callback( element ) {
    return function ( event ) {
      var isError = event.type === "error";
      // element.fadeOut(speed, function () {
        element.attr("src", isError ? null: event.target.src );
        // element.fadeIn(speed);
      // });
    };
  }
};

$(document).on("attentive-visible", "img[data-src]", function () {
  lazyload(this);
});

$(function () {

  $("img").each(function (index, img) {
    var el = $(img);
    el.attr("data-src", el.attr("src"));
    el.attr("src", "");
  });

});