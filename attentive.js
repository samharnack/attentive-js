var attentive = function (container, parent, childSelector) {
  var options = {
    "scroll-limit": 100,
    "event-offset": 100,
    "event-offset-multiplier": 0.5,
  };

  var debounce = function (fn, wait) {
    var timeout;
    return function () {
      var context = this;
      timeout = clearTimeout(timeout) || setTimeout(function () {
        fn.apply(context, arguments);
      }, wait);
    };
  };

  var containerRect = function (element) {
    var el = $(element);
    var isWindow = element instanceof Window;
    var pos = { top: isWindow ? el.scrollTop() : 0, left: isWindow ? el.scrollLeft() : 0 };
    return rect(pos, el.width(), el.height());
  };

  var childRect = function (element) {
    var el = $(element);
    return rect(el.position(), el.width(), el.height());
  };

  var rect = function (position, width, height) {
    return {
      top: position.top,
      bottom: position.top + height,
      left: position.left,
      right: position.left + width,
      width: width,
      height: height
    };
  };

  var collide = function (rect2, rect1) {
    return !(
      rect1.top > rect2.bottom ||
      rect1.right < rect2.left ||
      rect1.bottom < rect2.top ||
      rect1.left > rect2.right
    );
  };

  var isVisible = function (element, bounds) {
    var rect = childRect(element);
    var visible = collide(bounds, rect);
    // console.log(element, visible, rect, bounds);
    return visible;
  };

  var notify = function (event, wait, elements, arg) {
    if (!(elements instanceof Array)) {
      elements = [ elements ];
    }

    elements.forEach(function (element, index) {
      var delay = wait * (index + options["event-offset-multiplier"]);
      var fn = function () {
        $(element).trigger(event, arg || []);
      };

      return delay ? setTimeout(fn, delay) : fn();
    });
  };

  var children = function (element, childSelector) {
    if (children) {
      return $(element).find(childSelector).get();
    }

    return $(element).children().get();
  };

  var scrollHandler = function (container, childSelector, immediate) {
    var visible = [];
    var handler = debounce(function (event) {
      var elements = children(container, childSelector);
      var bounds = containerRect(this);
      var added = [], removed = [], existing = [];

      for (var index = 0, count = elements.length; index < count; index++) {
        var element = elements[index];
        var wasVisible = !!~visible.indexOf(element);
        var nowVisible = isVisible(element, bounds);

        if (nowVisible && wasVisible) {
          existing.push(element);
        }

        if (nowVisible && !wasVisible) {
          added.push(element);
        }

        if (!nowVisible && wasVisible) {
          removed.push(element);
        }
      }

      notify("attentive-hidden", 0, removed);
      notify("attentive-visible", options["event-offset"], added);

      visible = existing.concat(added);

    }, options["scroll-limit"]);

    if (immediate) handler.call(this);

    return handler;
  };

  var handler = scrollHandler(parent || "body", childSelector, true);
  $(container || window).on("scroll", handler);
  $(window).on("resize", handler);

  if (null === document.doctype) {
    console.error("WARNING: In order to properly detect visible window size, you must specify the doctype.")
  }
};